const puppeteer = require('puppeteer')
const fs = require('fs')

const url = 'https://reactjs.org/docs/getting-started.html'

puppeteer
    .launch()
    .then(function(browser) {
        return browser.newPage()
    })
    .then(function(page) {
        return page.goto(url).then(function() {
            return page.content()
        })
    })
    .then(function(html) {
        // fs.writeFileSync('page.txt', html)
        const headers = html.match(/(<h1|<h2|<h3)/gm)
        
        const headersCount = {
            h1: 0,
            h2: 0,
            h3: 0
        }

        headers.forEach((header) => {
            switch (header) {
                case '<h1': 
                    headersCount.h1++
                    break
                case '<h2':
                    headersCount.h2++
                    break
                case '<h3':
                    headersCount.h3++
                    break
            }
        })

        console.log(headersCount)
    })
    